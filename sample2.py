'+-------------------------------------------------------------------+'
'| = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = |'
'|{>/-------------------------------------------------------------\<}|'
'|: | Author:  Michael Ngugi                                      | :|'
'| :| Email:   micqualngugi96@gmail.com                           | :|'
'|: | Purpose: Parse Law Documents Using BeautifulSoup            | :|'
'|: | Date: 16-August-2017                                        | :|'
'| :| Version: 1.0                                                | :|'
'| :| Language: Python                                            | :|'
'| :| Script: (parser.py) functional text Extraction.             | :|'
'|{>\-------------------------------------------------------------/<}|'
'| = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = |'
'+-------------------------------------------------------------------+'

import mammoth
import json
import re
import Act
import pprint

def_docxfile = "ex.docx"

def getParts(html_table):
    sections = [[]]
    count = -1
    for tag in html_table.select('tr'):
        if tag.select('td p strong'):
            if re.search('Division', str(tag.text)):
                sections[count].append(sanitize_text(str(tag.text)))
                continue
            count = count + 1
            sections.append([])
            sections[count].append(sanitize_text(str(tag.text)))
        else:
            if tag.select('td[colspan=4] p') != []:
                sections[count].append(sanitize_text(str(tag.select('td[colspan=4] p'))))
    # d = {t: for t in sections[2]}
    count = 1;
    name = "Act"
    dict_parts = {}
    parts = []
    dict_parts['Content'] = []
    for part in sections:

        if count == 1:
            name = part[0]
            count = count + 1
            # print("The name is :" + name)
            continue
        parts.append(part)
        cnt = 1
        sub_part = {}
        sub_part_name = ""
        for p in part:
            if cnt == 1:
                sub_part_name = p
                sub_part[sub_part_name] = []
                cnt = cnt + 1
                continue
            sub_part[sub_part_name].append(p)
            cnt = cnt + 1
        dict_parts['Content'].append(sub_part)
        count = count + 1
    pp = pprint.PrettyPrinter(indent=4)
    # d = zip(part[0], part[1])
    dict_parts['Act Name'] = name
    # pp.pprint(dict_parts)




    with open('output-arrangment-of-Sections.txt','w') as output:
         output.write(json.dumps(dict_parts,indent=4))
    return

def sanitize_text(text):
    return text.replace(u'\u201c', '"') \
              .replace(u'\u201d', '"') \
              .replace(u'\u2019', '\'') \
              .replace(u'\u2013', '-')  \
              .replace(u'\u2014', ';')

def getPartBody(html_table_body):
    sections = [[]]
    count = 0
    flag = False
    cnt = 0
    #THe first array will contain the heading, that is until the search filter
    #gets the first Part
    sections[count].append("Heading:")
    #Loop through the table rows in the table looking for string PART
    for tag in html_table_body.find_all('tr'):
#        print(str(tag.next_sibling.text))

        if re.search('PART', str(tag.next_sibling)):
#            print('yADA yADA')
        #IF Part is found increment count and thus the array moves to the next index
            count = count + 1
        #add a new list to the array that will hold the sections belonging to the part
            sections.append([])
        #Append to the index with new List
            sections[count].append(sanitize_text(str(tag.next_sibling.text.replace('—', '-'))))
        #mark the flag indicating a new PART is found. This is important as it allows the loop to jump to the next iteration

            flag = True
            continue
#        print(tag.text)
        if tag.find('strong'):
            if flag:
                flag = False
                continue
            else:
                inner_section = [[]]

#       sections[count].append(inner_section)
#       !IMPORTANT. Okay, find the parent of the current element, then siblings of the parent, then use the sibling's text
#       to determine if its an empty arry or a string, Sections have to be named, Capisce!?
                if tag.find('strong').parent.parent.previous_sibling:
                    inner_sec = []
                    mysib = str(tag.find('strong').parent.parent.previous_sibling)
                    inner_sec.append(sanitize_text(mysib))
#                    print(tag.find('strong').parent.parent)
                    for sibling in tag.find('strong').parent.parent.next_siblings:
#                        print(sibling.text)
                        inner_sec.append(sanitize_text(str(sibling)))
                    inner_section[cnt].append(inner_sec)

                for sibling in tag.find('strong').parent.parent.parent.next_siblings:
                    inner_sec = []
                    inner_sec.append(sanitize_text(str(sibling)))
                    if sibling.find('strong'):
                        break
                    inner_section[cnt].append(inner_sec)

                inner_section.append(tag.find('strong').text.replace(".","".replace(" ","")))
                sections[count].append(inner_section)
    return sections

def parsetable(html_text):
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(html_text, 'html.parser')
    count = 1
    for table in soup.find_all('table'):
        if count == 1:
#            print("Yada")
            table.decompose()
            count = count + 1
            continue
        if count == 2:
#            print("Yada Yada")
            getParts(table)
            table.decompose()
            count = count + 1
            continue
        count = count + 1
    # getPartBody(soup)
    new_act = Act.Act()
    new_act.display(getPartBody(soup))
#    print(all_tables)
    return

with open(def_docxfile,"rb") as docx_file:
    result = mammoth.convert_to_html(docx_file)
    html = result.value
    messages = result.messages
    parsetable(html)

#Okay now the Foundation check, Now the real work begins
#Now User the multidimensional array to go through the document
