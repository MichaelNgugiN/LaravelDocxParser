import mammoth
import pprint
import json
import re

def_docxfile = "ex.docx"

def getParts(html_table):
    sections = [[]]
    count = -1
    for tag in html_table.select('tr'):
        if tag.select('td p strong'):
            if re.search('Division', str(tag.text)):
                sections[count].append(sanitize_text([str(tag.text)]))
                continue
            count = count + 1
            sections.append([])
            sections[count].append("part " + str(count) + ": " + sanitize_text(str(tag.text)))  
        else:
            if tag.select('td[colspan=4] p') != []:
                sections[count].append(sanitize_text(str(tag.select('td[colspan=4] p'))))       
    pp = pprint.PrettyPrinter(indent=4)
#    pp.pprint(sections[1][0]) 
#    pp.pprint(sections[1][1]) 
    with open('output-arrangment-of-Sections.txt','w') as output:
         output.write(json.dumps(sections,indent=4))
    return

def sanitize_text(text):
    return text.replace(u'\u201c', '"') \
              .replace(u'\u201d', '"') \
              .replace(u'\u2019', '\'') \
              .replace(u'\u2013', '-') 

def getPartBody(html_table_body):
    sections = [[]]
    count = 0
    flag = False
    cnt = 0
    #THe first array will contain the heading, that is until the search filter
    #gets the first Part
    sections[count].append("Heading:")
    #Loop through the table rows in the table looking for string PART
    for tag in html_table_body.find_all('tr'):
#        print(str(tag.next_sibling.text))
       
        if re.search('PART', str(tag.next_sibling)):
#            print('yADA yADA')
        #IF Part is found increment count and thus the array moves to the next index
            count = count + 1
        #add a new list to the array that will hold the sections belonging to the part
            sections.append([])
        #Append to the index with new List
            sections[count].append("part " + str(count) + ": " + sanitize_text(str(tag.next_sibling.text.replace('—', '-'))))
        #mark the flag indicating a new PART is found. This is important as it allows the loop to jump to the next iteration
      
            flag = True
            continue
#        print(tag.text)
        if tag.find('strong'):
            if flag:
                flag = False
                continue
            else:
                inner_section = [[]]
        
#       sections[count].append(inner_section)
#       !IMPORTANT. Okay, find the parent of the current element, then siblings of the parent, then use the sibling's text
#       to determine if its an empty arry or a string, Sections have to be named, Capisce!?
                if tag.find('strong').parent.parent.previous_sibling:
                    inner_sec = []
                    mysib = str(tag.find('strong').parent.parent.previous_sibling)
                    inner_sec.append(sanitize_text(mysib))
#                    print(tag.find('strong').parent.parent)
                    for sibling in tag.find('strong').parent.parent.next_siblings:
#                        print(sibling.text)
                        inner_sec.append(str(sibling))
                    inner_section[cnt].append(inner_sec)
                  
                for sibling in tag.find('strong').parent.parent.parent.next_siblings:
                    inner_sec = []
                    inner_sec.append(sanitize_text(str(sibling)))
                    if sibling.find('strong'):
                        break
                    inner_section[cnt].append(inner_sec)
               
                inner_section.append(tag.find('strong').text.replace(".","".replace(" ","")))
                sections[count].append(inner_section)              
#            print(tag.find('strong'))
#     print(str(tag.next_sibling.text))
#            for tg in tag:
#                print(tg.text)
        
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(sections[1])  
#    pp.pprint(sections[4][2][0])
    with open('output.txt','w') as output:
        output.write(json.dumps(sections,indent=4))
        print("DONE!")
#    pp.pprint(sections[2][2][0])       
    return
                      
def parsetable(html_text):
    from bs4 import BeautifulSoup 
    soup = BeautifulSoup(html_text, 'html.parser')
    count = 1
    for table in soup.find_all('table'):
        if count == 1:
#            print("Yada")
            table.decompose()
            count = count + 1
            continue
        if count == 2:
#            print("Yada Yada")
            getParts(table)
            table.decompose()
            count = count + 1
            continue
        count = count + 1
    getPartBody(soup)
#    print(all_tables)
    return

with open(def_docxfile,"rb") as docx_file:
    result = mammoth.convert_to_html(docx_file)
    html = result.value
    messages = result.messages
    parsetable(html)
    
#Okay now the Foundation check, Now the real work begins
#Now User the multidimensional array to go through the document

class Act:
    def __init__(self):
        print('Yada Yada')
