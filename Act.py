'+-------------------------------------------------------------------+'
'| = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = |'
'|{>/-------------------------------------------------------------\<}|'
'|: | Author:  Michael Ngugi                                      | :|'
'| :| Email:   micqualngugi96@gmail.com                           | :|'
'|: | Purpose: Parse Law Documents Using BeautifulSoup            | :|'
'|: | Date: 16-August-2017                                        | :|'
'| :| Version: 1.0                                                | :|'
'| :| Language: Python                                            | :|'
'| :| Script: [class](Act.py) ACT Definition.                     | :|'
'|{>\-------------------------------------------------------------/<}|'
'| = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = : = |'
'+-------------------------------------------------------------------+'

import pprint
import json

class Act:
    'now, Let the magic happen'
    def __init__(self):
        print('Yada Yada')
    def display(self,sections):
        # print(sections[1])
        pp = pprint.PrettyPrinter(indent=4)
        # contains the name of a section
        # pp.pprint(sections[14][1][0][0][0])
        # pp.pprint(sections[14][1][0]) #body of a section
        # pp.pprint(sections[14][1][1]) #number(Index) of a section
        # pp.pprint(sections[14][2]) #Second Section section of a part Content
        # d = {k: v for sub_l in sections[1] for k, *v in sub_l}
        d = {t[0]:t[1:] for t in sections}
        with open('output.txt','w') as output:
            output.write(json.dumps(d,indent=4))
            print("DONE!")
