{
    "Content": [
        {
            "PART I - PRELIMINARY": [
                "[<p>Short title and commencement.</p>]",
                "[<p>Interpretation.</p>]",
                "[<p>Open market value.</p>]",
                "[<p>Time of supply or importation.</p>]"
            ]
        },
        {
            "PART II- LIABILITY FOR EXCISE DUTY": [
                "[<p>Imposition of excise duty.</p>]",
                "[<p>Timing of liability for excise duty.</p>]",
                "[<p>Good and services not liable to excise duty.</p>]",
                "[<p>Variation of rates of excise duty.</p>]",
                "[<p>Excisable value.</p>]",
                "[<p>Adjustment for inflation.</p>]",
                "[<p>Ex-factory selling price of excisable goods.</p>]",
                "[<p>Quantity of excisable goods.</p>]",
                "[<p>Place of supply of excisable services.</p>]",
                "[<p>Relief for raw materials.</p>]"
            ]
        },
        {
            "PART III- LICENSING": [
                "[<p><em>A-Application for a Licence</em></p>]",
                "[<p>Activities requiring a licence.</p>]",
                "[<p>Applications for a licence.</p>]",
                "[<p><em>B-Issue of Licences</em></p>]",
                "[<p>Issue of licence.</p>]",
                "[<p>Form of Licence.</p>]",
                "[<p>Obligations of licensed person.</p>]",
                "[<p><em>C;Suspension and Cancellation of Licences</em></p>]",
                "[<p>Suspension of licence.</p>]",
                "[<p>Cancellation of licence.</p>]",
                "[<p>Consequences of suspension or cancellation of licence.</p>]",
                "[<p>Commissioner to notify licensee prior to suspension, revocation, cancellation or refusal of renewal of a licence.</p>]"
            ]
        },
        {
            "PART IV;EXCISE CONTROL": [
                "[<p>Excisable goods under excise control.</p>]",
                "[<p>Obligations of licensed manufacturer in relation to excisable goods under excise control.</p>]",
                "[<p>Keeping or use of still otherwise than by distiller or rectifier prohibited.</p>]",
                "[<p>Deemed removal of excisable goods.</p>]"
            ]
        },
        {
            "PART V-EXCISE STAMPS": [
                "[<p>Excise stamps and other markings.</p>]"
            ]
        },
        {
            "PART VI-REFUNDS": [
                "[<p>Refunds.</p>]",
                "[<p>Excisable goods subject to a refund liable for excise duty on disposal or inconsistent use.</p>]",
                "[<p>Exempt excisable goods liable for excise duty on re-importation or purchase.</p>]",
                "[<p>Exempt excisable goods liable for excise duty on disposal or inconsistent use.</p>]"
            ]
        },
        {
            "PART VII-EXCISE DUTY PROCEDURE": [
                "[<p>Application of Tax Procedures Act.</p>]",
                "[<p>Record keeping.</p>]",
                "[<p>Excise duty returns.</p>]",
                "[<p>Payment of excise duty.</p>]",
                "[<p>Security.</p>]"
            ]
        },
        {
            "PART VIII-OFFENCES AND PENALTIES": [
                "[<p>Penalty</p>]",
                "[<p>Offences relating to licensing and excise control.</p>]",
                "[<p>Offences relating to excise stamps.</p>]",
                "[<p>Sanctions for offences.</p>]"
            ]
        },
        {
            "PART IX-MISCELLANEOUS PROVISIONS": [
                "[<p>Tax avoidance schemes.</p>]",
                "[<p>Effect on prices of imposition, abolition, or variation of excise duty.</p>]",
                "[<p>Application of East African Community Customs Management, Act 2004.</p>]",
                "[<p>Regulations.</p>]",
                "[<p>Repeals and transitional provisions.</p>]"
            ]
        },
        {
            "SCHEDULES": []
        },
        {
            "SUBSIDIARY LEGISLATION": []
        },
        {
            "The Excise Duty (Remission of Excise Duty) Regulations, 2017       [under sec 7(2)]": []
        },
        {
            "The Excise Duty (Excisable Goods Management                                      [under sec 45]System) Regulations, 2017": []
        },
        {}
    ],
    "Act Name": "THE EXCISE DUTY ACTNo. 23 of 2015ARRANGEMENT OF SECTIONS"
}